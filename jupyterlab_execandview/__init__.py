from notebook.notebookapp import NotebookApp
from jupyterlab_execandview.handlers import setup_handlers


# server extension for execandview
def _jupyter_server_extension_paths():
    return [{"module": "jupyterlab_execandview"}]


def load_jupyter_server_extension(notebook_app: NotebookApp):
    setup_handlers(notebook_app.web_app)
