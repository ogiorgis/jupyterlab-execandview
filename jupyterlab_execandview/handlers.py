import nbformat
from nbconvert import HTMLExporter
from nbconvert.preprocessors import ExecutePreprocessor, TagRemovePreprocessor
from notebook.notebookapp import NotebookWebApplication
from notebook.utils import url_path_join
from notebook.base.handlers import APIHandler, path_regex
from traitlets.config import Config


def setup_handlers(web_app: NotebookWebApplication) -> None:
    host_pattern = ".*$"
    web_app.add_handlers(
        host_pattern,
        [
            (
                url_path_join(
                    web_app.settings["base_url"],
                    "/exec_and_view%s" % path_regex,
                ),
                FormatAPIHandler,
            )
        ],
    )


def to_html(nb_filename):
    ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
    with open(nb_filename) as f:
        nb_content = nbformat.read(f, as_version=4)
    ep.preprocess(nb_content)
    html_exporter = HTMLExporter()
    html_exporter.exclude_input = True
    html_exporter.exclude_input_prompt = True
    (body, _) = html_exporter.from_notebook_node(nb_content)
    html_filename = nb_filename.replace('.ipynb', '.html')
    with open(html_filename, "w") as f:
        f.write(body)
    return html_filename


class FormatAPIHandler(APIHandler):

    def get(self, path=''):
        new_file = to_html(path[1:])
        self.redirect(f"../view/{new_file}")
