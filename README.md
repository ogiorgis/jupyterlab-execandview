# JupyterLab ExecAndView

## Installation

First clone the repo
```bash
git clone https://gitlab.com/logilab/jupyterlab-execandview
cd jupyterlab-execandview
python setup.py install
jupyter serverextension enable --py jupyterlab_execandview
```
